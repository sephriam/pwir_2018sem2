#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<string.h>
#include<time.h>
#include<sys/wait.h>
#include <unistd.h>
#include <ctime>
#include <bits/types/time_t.h>
#include <cstdlib>
#include <bits/types/struct_timespec.h>

using namespace std;

#include"error.h"
#include "semaphore.h"

void copy_delayed(const char *txt, char *buf, unsigned int seed) {
    srand(seed);
    struct timespec ts{};
    int i;
    for (i = 0; txt[i] != '\0'; i++) {
        ts.tv_sec = 0;
        ts.tv_nsec = (long) (((double) rand()) / RAND_MAX * 9999990);
        nanosleep(&ts, NULL);
        buf[i] = txt[i];
    }
    buf[i] = '\0';
}

int open_sem(const char *f,int nsems,int semflg){
    key_t key = ftok(f,1);
    if(key==-1) error_exit("Błąd ftok");
    int id = semget(key,nsems,semflg);
    if(id==-1) error_exit("Błąd semget");
    return id;
}

int main(int argc, char *argv[]) {
    if (argc != 3) error_exit("Zła ilość argumentów");
    /*           Dylan Thomas, 1914-1953
       „Do not go gentle into that good night” */
    const char *lines[] = {
            "Do not go gentle into that good night,",
            "Old age should burn and rave at close of day;",
            "Rage, rage against the dying of the light.",

            "Though wise men at their end know dark is right,",
            "Because their words had forked no lightning they",
            "Do not go gentle into that good night.",

            "Good men, the last wave by, crying how bright",
            "Their frail deeds might have danced in a green bay,",
            "Rage, rage against the dying of the light.",

            "Wild men who caught and sang the sun in flight,",
            "And learn, too late, they grieved it on its way,",
            "Do not go gentle into that good night.",

            "Grave men, near death, who see with blinding sight",
            "Blind eyes could blaze like meteors and be gay,",
            "Rage, rage against the dying of the light.",

            "And you, my father, there on the sad height,",
            "Curse, bless, me now with your fierce tears, I pray.",
            "Do not go gentle into that good night.",
            "Rage, rage against the dying of the light."
    };
    const int N = 19;
    if (argc != 3) error_exit("Błędne argumenty");
    key_t key = ftok(argv[1], 1);
    if (key == -1) error_exit("Błąd ftok");
    int id = shmget(key, 1024, IPC_CREAT | 0600);
    if (id == -1)
        error_exit("Błąd przy tworzeniu/otwieraniu segmentu");
    char *buf = (char *) shmat(id, 0, 0);
    if ((void *) buf == (void *) -1)
        error_exit("Błąd przy doczepianiu segmentu");
    time_t t = time(NULL);

    int sem_id = open_sem(argv[2],1,IPC_EXCL|IPC_CREAT|0600);
    //key_t sem_key = ftok(argv[1],1);
    semaphore sem(sem_id, 0, 1);

    for (int i = 0; i < N; i++) {
        if (fork() == 0) {
            sem.Wait();
            copy_delayed(lines[i], buf, i + t);
            sem.Signal();
            return 0;
        }
    }
    int status;
    for (int i = 0; i < N; i++) wait(&status);
    cout << buf << "\n";

    open_sem(argv[2],0,0);
    return 0;
}