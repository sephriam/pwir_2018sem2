#include<stdlib.h>
#include<errno.h>
 
#ifndef __eexit
#define __eexit
 
void error_exit(const char *text){
   if(errno==0&&text!=0) cerr<<text<<"\n";
   else perror(text);
   exit(1);
}
 
#endif