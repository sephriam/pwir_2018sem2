#include<sys/ipc.h>
#include<sys/sem.h>
#include "error.h"



#ifndef __my_semaphore__
#define __my_semaphore__
class semaphore {
    int id;
    int semnum;
public:
    /*  -----------------------------------
                  Konstruktory
        -----------------------------------
       Argumenty:
          _id:     identyfikator banku semaforów,
          _semnum: indeks semafora w banku,
          val:     wartosc początkowa.
    */
    semaphore(int _id,int _semnum,int val):id(_id),semnum(_semnum){
        if(semctl(id,semnum,SETVAL,val))
            error_exit("Error while setting semaphore value");
    }
    semaphore(int _id,int _semnum):id(_id),semnum(_semnum){}

    semaphore():id(-1),semnum(-1){}

    /*  ----------------------------------
                     Metody
        ---------------------------------- */

    int getid() const {return id;}

    int value() const{
        int val=semctl(id,semnum,GETVAL,0);
        if(val==-1)
            error_exit("Error while reading semaphore value");
        return val;
    }


    /*
        Sygnalizowanie semafora:
        Wartość n zostanie dodana do aktualnej wartości semafora.
    */
    semaphore &Signal(int n = 1,int flag=0){
        sembuf op;
        op.sem_num=semnum;
        op.sem_flg=flag;
        op.sem_op=n;
        if(semop(id,&op,1))
            error_exit("Error in semaphore::Signal()");
        return *this;
    }
    /*
        Czekanie pod semaforem:
        Wartość n zostanie odjęta od aktualnej wartości semafora.
    */
    semaphore &Wait(int n = 1,int flag=0){
        sembuf op;
        op.sem_num=semnum;
        op.sem_flg=flag;
        op.sem_op=-n;
        if(semop(id,&op,1))
            error_exit("Error in semaphore::Wait()");
        return *this;
    }

    /*  ----------------------------------
                     Operatory
        ---------------------------------- */

    /* Ustawia wartość semafora */
    semaphore &operator=(int n){
        if(semctl(id,semnum,SETVAL,n)==-1)
            error_exit("Error while setting semphore value");
        return *this;
    }

};
#endif