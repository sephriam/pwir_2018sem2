#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<string.h>

using namespace std;

void error_exit(const char *text){
    if(errno==0&&text!=0) cerr<<text<<"\n";
    else perror(text);
    exit(1);
}


int open_sem(const char *f,int nsems,int semflg){
    key_t key = ftok(f,1);
    if(key==-1) error_exit("Błąd ftok");
    int id = semget(key,nsems,semflg);
    if(id==-1) error_exit("Błąd semget");
    return id;
}

long get_num(const char *txt){
    char *tail;
    long n=strtol(txt,&tail,10);
    if(tail[0]!='\0')
        error_exit("Argument powinien być liczbą dziesietną");
    return n;
}

void help(){
    cerr<<"Sposób użycia: \nsemtest plik komenda\n"
        <<"Gdzie plik zostanie użyty przez ftok()"
        <<"a komenda może być jedną z:\n"
        <<"   create n    -tworzy bank n semaforów\n"
        <<"   delete      -kasuje bank\n"
        <<"   list        -wylistowuje aktualne "
        <<                "wartości semaforów\n"
        <<"   op n m      -dodaje m (ze znakiem) "
        <<                 "do n-tego semafora\n";
    exit(0);
}

int main(int argc,char *argv[]){
    if(argc<3) help();
    if(strcmp(argv[2],"create")==0){
        if(argc!=4) help();
        int nsems=get_num(argv[3]);
        open_sem(argv[1],nsems,IPC_EXCL|IPC_CREAT|0600);
        return 0;
    }
    if(strcmp(argv[2],"delete")==0){
        if(argc!=3) help();
        int id=open_sem(argv[1],0,0);
        if(semctl(id,0,IPC_RMID,0))
            error_exit("Błąd przy usuwaniu banku semaforów");
        return 0;
    }
    if(strcmp(argv[2],"list")==0){
        if(argc!=3) help();
        int id=open_sem(argv[1],0,0);
        //Get the number of semaphores:
        struct semid_ds buf;
        if(semctl(id,0,IPC_STAT,&buf)==-1)
            error_exit("Błąd przy znajdowaniu ilości semaforów");
        int nsems=buf.sem_nsems;
        ushort *arr=new ushort[nsems];
        //W tablicy arr  funkcja semctl()
        //umieści aktualne wartości semaforów
        if(semctl(id,0,GETALL,arr)==-1)
            error_exit("Blad semctl");
        cout<<"\nWartosci semaforow:\n\n";
        for(int i=0;i<nsems;i++){
            cout<<"  sem["<<i<<"]="<<arr[i]<<"\n";
        }
        cout<<"\n\n";
        return 0;
    }
    if(strcmp(argv[2],"op")==0){
        if(argc!=5) help();
        int id=open_sem(argv[1],0,0);
        struct sembuf sbuf;
        sbuf.sem_num=get_num(argv[3]);
        sbuf.sem_op=get_num(argv[4]);
        sbuf.sem_flg=0;
        if(semop(id,&sbuf,1)==-1)
            error_exit("Błąd semop");
        return 0;
    }
    help();
    return 0;
}