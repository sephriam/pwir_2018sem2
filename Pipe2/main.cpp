#include<iostream>
#include<unistd.h>
#include<sys/wait.h>

using namespace std;

int main() {
    int mypipe[2];
    if (pipe(mypipe)) { //Tworzymy rurke.
        cout << "Nie udalo sie utworzyc rurki\n";
        return 1;
    }

    int mypipe2[2];
    if (pipe(mypipe2)) { //Tworzymy rurke.
        cout << "Nie udalo sie utworzyc rurki\n";
        return 1;
    }

    double a, b;
    pid_t pid = fork();
    if (pid == 0) {
        close(mypipe[1]);//Zamykamy niepotrzebny koniec
        close(mypipe2[0]);//Zamykamy niepotrzebny koniec
        read(mypipe[0], &a, sizeof(double));
        read(mypipe[0], &b, sizeof(double));
        cout << "Odczytalem dane a=" << a
             << " b=" << b << " a+b=" << a + b << "\n";
        a += b;
        a /= 2.0;
        cout << "Wysylam " << a << '\n';
        write(mypipe2[1], &a, sizeof(double));
        return 0;
    }
    close(mypipe[0]);//Zamykamy niepotrzebny koniec
    close(mypipe2[1]);//Zamykamy niepotrzebny koniec
    cout << "Podaj a=";
    cin >> a;
    cout << "Podaj b=";
    cin >> b;
    write(mypipe[1], &a, sizeof(double));
    write(mypipe[1], &b, sizeof(double));
    cout << "Wyslalem dane,\n"
         << "czekam na wynik od dziecka....\n";

    read(mypipe2[0], &a, sizeof(double));
    cout << "Srednia od dziecka: " << a << '\n';

    int status;
    wait(&status);
    return 0;
}