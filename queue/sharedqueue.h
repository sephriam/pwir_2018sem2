//
// Created by wojtowic on 12.05.18.
//

#ifndef QUEUE_SHAREDQUEUE_H
#define QUEUE_SHAREDQUEUE_H

#include "semaphore.h"
#include <unistd.h>
#include<sys/ipc.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include <cstring>

class sharedqueue {

public:

    //Rozmiar pojedyńczej wiadomości
    static const int msg_size = 100;

private:
    const int size = 0;  //Rozmiar kolejki
    const int shmid = 0; //ipc id banku semaforów
    const int semid = 0; //ipc id segmentu dzielonego

    int nitems = 0;  //Ilość elementów aktualnie w kolejce
    int write_at = 0;//Umieszczamy wiadomość w buf[write_at]
    int read_at = 0; //Pobieramy wiadomość z buf[read_at]

    //Typ semaphore zdefiniowany na poprzednim wykładzie
    semaphore mutex;
    semaphore spaces;
    semaphore items;

    //Prywatny konstruktor. Wywoływany ze
    //statycznej funkcji open()
    sharedqueue(int _size, int _shmid, int _semid)
            : size(_size), shmid(_shmid), semid(_semid),
              write_at(0), read_at(0), nitems(0),
              mutex(_semid, 0, 1), spaces(_semid, 1, _size),
              items(_semid, 2, 0) {}

public:

    //Zwróć nitems
    int get_nitems() {
        return items.value();
    }

    //Umieść w kolejce łańcuch znaków msg.
    void send(const char *msg) {

        spaces.Wait();
        mutex.Wait();

        char *wptr = (char *) this + sizeof(*this) + (write_at * msg_size);

        size_t len = strlen(msg);

        if (len > msg_size) {
            memcpy(wptr, msg, msg_size);
            wptr[msg_size] = '\0';
        } else {
            memcpy(wptr, msg, len);
        }

        ++write_at;
        write_at %= size;

        mutex.Signal();
        items.Signal();
    }

    //Pobierz z kolejki wiadomość
    //i umieść ją w tablicy msg.
    //Pobieranie wiadomości usuwa ją z kolejki.
    void receive(char *msg) {

        items.Wait();
        mutex.Wait();

        char *rptr = (char *) this + sizeof(*this) + (read_at * msg_size);
        memcpy(msg, rptr, msg_size);

        ++read_at;
        read_at %= size;

        mutex.Signal();
        spaces.Signal();
    }

    //Usuń z pamięci segment dzielony i bank semaforów
    //o kluczu ipc key.
    static void remove(key_t key) {
        int sem_id = semget(key, 0, 0);
        if (sem_id == -1) error_exit("Błąd semget");
        semctl(sem_id, 0, IPC_RMID, 0);

        int shm_id = shmget(key, 0, 0);
        if (shm_id == -1)
            error_exit("Błąd przy tworzeniu/otwieraniu segmentu");
        shmctl(shm_id, 0, IPC_RMID);
    }

    //Zwróć wskaźnik na obiekt sharedqueue umieszczony
    //na początku segmentu dzielonego o kluczu key.
    //W przypadku gdy create jest true należy:
    //   1. Stworzyć segment dzielony i 3-elementowy bank
    //      semaforów o kluczu key.
    //   2. Doczepić segment dzielony do pamięci i korzystając
    //      z placement new umieścić na początku segmentu nowy
    //      obiekt shared queue. Rozmiar kolejki jest dany
    //      przez _size (należy ten parametr wziąść po uwagę
    //      także przy tworzeniu segmentu).
    //W przypadku gdy create jest false należy doczepić do
    //pamięci segment dzielony o kluczu key (gdy taki segment
    //nie istnieje należy zgłosić błąd) a następnie jedynie
    //zrzutować (bez wywoływania konstruktora) otrzymany
    //wskaźnik na początek segmentu na typ sharedqueue.
    static sharedqueue *open(key_t key, int _size, bool create) {

        int id = shmget(key, sizeof(sharedqueue) + (_size * msg_size), IPC_CREAT | 0600);
        if (id == -1)
            error_exit("Błąd przy tworzeniu/otwieraniu segmentu");
        sharedqueue *buf = (sharedqueue *) shmat(id, 0, 0);
        if ((void *) buf == (void *) -1)
            error_exit("Błąd przy doczepianiu segmentu");

        int sem_id = semget(key, 3, IPC_CREAT | 0600);

        if (create) {
            buf = new(buf) sharedqueue(_size, id, sem_id);
        }

        return buf;
    }

};

#endif //QUEUE_SHAREDQUEUE_H
