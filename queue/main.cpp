#include <iostream>
#include <sys/ipc.h>
#include "error.h"
#include "sharedqueue.h"

int main(int argc, char* argv[]) {

    key_t key = ftok(argv[1], 1);
    if (key == -1) error_exit("Błąd ftok");

    std::string command = std::string(argv[2]);

    if(command == "create") {
        sharedqueue::open(key, std::stoi(argv[3]), true);
    } else if(command == "delete") {
        sharedqueue::remove(key);
    } else if(command == "receive") {
        char buff[1024];
        sharedqueue* queue = sharedqueue::open(key, 1024, false);
        queue->receive(buff);
        printf("%s\n", buff);
    } else if(command == "send") {
        sharedqueue* queue = sharedqueue::open(key, 1024, false);
        queue->send(argv[3]);
    }

    return 0;
}