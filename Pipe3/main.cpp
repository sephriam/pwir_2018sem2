#include<iostream>
#include<unistd.h>
#include<sys/wait.h>
#include "fdstream.h"

using namespace std;

int main(){
    int status, mypipe[2];
    if(pipe(mypipe)){ //Tworzymy rurke.
        cerr<<"Nie udalo sie utworzyc rurki\n";
        return 1;
    }
    pid_t pid=fork();
    if(pid==0){
        close(mypipe[1]);
        dup2(mypipe[0],STDIN_FILENO);
        //Odtąd STDIN_FILENO to to samo co mypipe[0].
        execl("/usr/bin/less","less",NULL);
        cerr<<"Nie udalo sie uruchomic less\n";
        return 1;
    }
    close(mypipe[0]);
    {
        boost::fdostream o(mypipe[1]);
        for(int i=0;i<1000;i++) o<<"i="<<i<<"\n";
        o.flush();
    }
    close(mypipe[1]);
    wait(&status);
    cerr<<"Kończę działanie...\n";
    return 0;
}