#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

using namespace std;

int zero() {
    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        int c;
        cout<<"Podaj wartość zwracaną: ";cin>>c;
        return c;
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "<<"status="
        <<WEXITSTATUS(ret_status)<<"\n";
}

int one() {
    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        while(true);
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "<<"status="
        <<WEXITSTATUS(ret_status)<<"\n";
}

int two() {
    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        return 0;
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    while(true);
}

int three() {
    pid_t pid;
    for(int i = 0; i < 10; ++i) {
        pid=fork();
        if (pid == 0) {
            cout << "Jestem dzieckiem, moj PID=" << getpid()
                 << ",\n pid mojego ojca to: " << getppid() << "\n";
            while (true);
        }
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "<<"status="
        <<WEXITSTATUS(ret_status)<<"\n";
}

int four(int i = 0) {

    while(i == 10) {

    }

    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        return four(++i);
    }

    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "<<"status="
        <<WEXITSTATUS(ret_status)<<"\n";

    return ret_status;
}

int five() {
    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        return execl("/usr/bin/gedit","gedit", nullptr);
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "
        <<"status="<<WEXITSTATUS(ret_status)<<"\n";
    return 0;
}

int six() {
    pid_t pid;
    for(int i = 0; i < 10; ++i) {
        pid = fork();
        if (pid == 0) {
            cout << "Jestem dzieckiem, moj PID=" << getpid()
                 << ",\n pid mojego ojca to: " << getppid() << "\n";
            return execl("/usr/bin/gedit", "gedit", nullptr);
        }
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "
        <<"status="<<WEXITSTATUS(ret_status)<<"\n";
    return 0;
}

int seven(char** argv) {

    int i = atoi(argv[1]);
    while(i <= 0);

    pid_t pid;
    pid=fork();
    if(pid==0){
        cout<<"Jestem dzieckiem, moj PID="<<getpid()
            <<",\n pid mojego ojca to: "<<getppid()<<"\n";
        sprintf(argv[1], "%d", --i);
        return execl(argv[0], argv[0], argv[1], nullptr);
    }
    cout<<"Jestem ojcem, moj PID="<<getpid()
        <<",\n PID mojego dziecka to: "<<pid<<"\n";
    int ret_status;
    wait(&ret_status);
    cout<<"Dziecko zakonczylo dzialanie "
        <<"status="<<WEXITSTATUS(ret_status)<<"\n";
    return 0;
}

int main(int argc, char** argv){

    //return zero();
    //return one();
    //return two();
    //return three();
    //return four();

    //return five();
    //return six();
    return seven(argv);
}