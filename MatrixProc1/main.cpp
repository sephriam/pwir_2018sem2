#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cerrno>
#include<sys/ipc.h>
#include<sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>

struct matrices {
    static const int M = 3;
    static const int N = 3;
    static const int K = 3;
    double A[M][K];
    double B[K][N];
    double C[M][N];
};

/* Pomocnicza funkcja do obsługi błędów */
void error_exit(const char *text) {
    if (errno == 0 && text != 0) {
        std::cerr << text << "\n";
    } else {
        perror(text);
    }
    exit(1);
}

int main(int argc, char *argv[]) {
    key_t key = ftok(argv[0], 1);
    if (key == -1) error_exit("Błąd ftok");

    /* Tworzymy/otwieramy segment pamieci dzielonej */
    int id = shmget(key, sizeof(matrices), IPC_CREAT | 0600);
    if (id == -1)
        error_exit("Błąd przy otwieraniu segmentu");
    /* Podczepiamy segment pamięci dzielonej do
       przestrzeni adresowej programu. Odtąd zmienna
       buf pokazuje na początek tego segmentu */
    matrices *mat = (matrices *) shmat(id, 0, 0);
    if (mat == (matrices *) -1)
        error_exit("Błąd przy doczepianiu segmentu");
    std::cout << "key=" << key << "  id=" << id
              << " pid=" << getpid() << "\n";

    std::cout << "Macierz A\n";
    for (size_t i = 0; i < matrices::M; ++i) {
        for (size_t j = 0; j < matrices::K; ++j) {
            //std::cin >> mat->A[i][j];
            mat->A[i][j] = i * j;
        }
    }

    std::cout << "Macierz B\n";
    for (size_t i = 0; i < matrices::K; ++i) {
        for (size_t j = 0; j < matrices::N; ++j) {
            //std::cin >> mat->B[i][j];
            mat->B[i][j] = i * j;
        }
    }

    size_t col = 0;
    size_t row = 0;
    pid_t pid = 0;
    do {
        pid = fork();
        std::cout << pid << " row: " << row << " col: " << col << '\n';
        ++col;
        if (col % matrices::N == 0) {
            ++row;
            col = 0;
        }

    } while (pid != 0 && row < matrices::M && col < matrices::N);

    if (pid == 0) {
        mat->C[row][col] = 0;
        for (size_t i = 0; i < matrices::N; ++i) {
            mat->C[row][col] += mat->A[row][i] * mat->B[i][col];
        }
    } else {
        for (size_t i = 0; i < matrices::M * matrices::N; ++i) {
            int ret_status;
            wait(&ret_status);
            std::cout << "Dziecko zakonczylo dzialanie "
                      << "status=" << WEXITSTATUS(ret_status) << "\n";
        }

        std::cout << "Macierz A\n";
        //    double A[M][K];
        //    double B[K][N];
        //    double C[M][N];
        for (size_t i = 0; i < matrices::M; ++i) {
            for (size_t j = 0; j < matrices::K; ++j) {
                std::cout << mat->A[i][j] << '\t';
            }
            std::cout << '\n';
        }

        std::cout << "Macierz B\n";
        for (size_t i = 0; i < matrices::K; ++i) {
            for (size_t j = 0; j < matrices::N; ++j) {
                std::cout << mat->B[i][j] << '\t';
            }
            std::cout << '\n';
        }

        std::cout << "Macierz C\n";
        for (size_t i = 0; i < matrices::M; ++i) {
            for (size_t j = 0; j < matrices::N; ++j) {
                std::cout << mat->C[i][j] << '\t';
            }
            std::cout << '\n';
        }
    }

    return 0;
}