#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cerrno>
#include<sys/ipc.h>
#include<sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include "SegmentManager.h"

/* Pomocnicza funkcja do obsługi błędów */
void error_exit(const char *text) {
    if (errno == 0 && text != 0) {
        std::cerr << text << "\n";
    } else {
        perror(text);
    }
    exit(1);
}

int main(int argc, char *argv[]) {
    key_t key = ftok(argv[0], 1);
    if (key == -1) error_exit("Błąd ftok");

    int M = 4;
    int K = 4;
    int N = 4;

    std::cout << "M: ";
    //std::cin >> M;

    std::cout << "K: ";
    //std::cin >> K;

    std::cout << "N: ";
    //std::cin >> N;

    /* Tworzymy/otwieramy segment pamieci dzielonej */
    // | 4M | 4K | 4N | 8*M*K AAAAAA | 8*K*N BBBBB | 8*M*N CCCCC |
    int id = shmget(key, sizeof(double)*M*K + sizeof(double)*K*N + sizeof(double)*M*N, IPC_CREAT | 0600);
    if (id == -1)
        error_exit("Błąd przy otwieraniu segmentu");
    /* Podczepiamy segment pamięci dzielonej do
       przestrzeni adresowej programu. Odtąd zmienna
       buf pokazuje na początek tego segmentu */
    double *S = (double *) shmat(id, 0, 0);
    if (S == (double *) -1)
        error_exit("Błąd przy doczepianiu segmentu");
    std::cout << "key=" << key << "  id=" << id
              << " pid=" << getpid() << "\n";

    SegmentManager manager(M, K, N, S);

    std::cout << "Macierz A\n";
    Matrix A = manager.getA();
    for (size_t i = 0; i < A.getM(); ++i) {
        for (size_t j = 0; j < A.getN(); ++j) {
            //std::cin >> mat->A[i][j];
            A(i,j) = i * j;
        }
    }

    std::cout << "Macierz B\n";
    Matrix B = manager.getB();
    for (size_t i = 0; i < B.getM(); ++i) {
        for (size_t j = 0; j < B.getN(); ++j) {
            //std::cin >> mat->A[i][j];
            B(i,j) = i * j;
        }
    }

    size_t col = 0;
    size_t row = 0;
    size_t items = 0;
    pid_t pid = 0;
    do {
        pid = fork();
        if(pid != 0) {
            ++items;
        } else {
            row = items / N;
            col = items % N;
            std::cout << " row: " << row << " col: " << col << '\n';
        }

    } while (pid != 0 && items < M*N);

    Matrix C = manager.getC();
    if (pid == 0) {
        C(row,col) = 0;
        for (size_t i = 0; i < K; ++i) {
            C(row,col) += A(row,i) * B(i,col);
        }
        std::cout << "[" << row << ',' << col << "] = " << C(row,col) << '\n';
    } else {
        for (size_t i = 0; i < M * N; ++i) {
            int ret_status;
            wait(&ret_status);
            /*std::cout << "Dziecko zakonczylo dzialanie "
                      << "status=" << WEXITSTATUS(ret_status) << "\n";*/
        }

        std::cout << "Macierz A\n";
        //    double A[M][K];
        //    double B[K][N];
        //    double C[M][N];
        for (size_t i = 0; i < A.getM(); ++i) {
            for (size_t j = 0; j < A.getN(); ++j) {
                std::cout << A(i,j) << '\t';
            }
            std::cout << '\n';
        }

        std::cout << "Macierz B\n";
        for (size_t i = 0; i < B.getM(); ++i) {
            for (size_t j = 0; j < B.getN(); ++j) {
                std::cout << B(i,j) << '\t';
            }
            std::cout << '\n';
        }

        std::cout << "Macierz C\n";
        for (size_t i = 0; i < C.getM(); ++i) {
            for (size_t j = 0; j < C.getN(); ++j) {
                std::cout << C(i,j) << '\t';
            }
            std::cout << '\n';
        }
    }

    return 0;
}