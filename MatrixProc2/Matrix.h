//
// Created by wojtowic on 21.04.18.
//

#ifndef MATRIXPROC2_MATRIX_H
#define MATRIXPROC2_MATRIX_H


class Matrix {
    const int M;
    const int N;
    double *const A;
public:
    Matrix(int _M, int _N, double *_A) : M(_M), N(_N), A(_A) {

    }

    int getM() const { return M; }

    int getN() const { return N; }

    double &operator()(size_t m, size_t n) {
        return *(A + (m * N) + n);
    }

    double operator()(size_t m, size_t n) const {
        return *(A + (m * N) + n);
    }
};


#endif //MATRIXPROC2_MATRIX_H
