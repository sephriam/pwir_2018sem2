//
// Created by wojtowic on 21.04.18.
//

#ifndef MATRIXPROC2_SEGMENTMANAGER_H
#define MATRIXPROC2_SEGMENTMANAGER_H


#include "Matrix.h"

class SegmentManager {

    // A --> M x K
    // B --> K x N
    // C --> M x N
    const int M;
    const int K;
    const int N;
    //Od tego miejsca przechowywane są elementy tablic A, B i C.
    double *S;
public:
    SegmentManager(int _M, int _K, int _N, double* _S) : M(_M), K(_K), N(_N), S(_S) {

    }


    // | 4M | 4K | 4N | 8*M*K AAAAAA | 8*K*N BBBBB | 8*M*N CCCCC |

    //Zwraca macierz A:
    Matrix getA() {
        return Matrix(M, K, S);
    }

    //Zwraca macierz B. Do samodzielnej implementacji.
    Matrix getB() {
        return Matrix(K, N, S + (M * K));
    }

    //Zwraca macierz C. Do samodzielnej implementacji.
    Matrix getC() {
        return Matrix(M, N, S + (M * K) + (K * N));
    }
};


#endif //MATRIXPROC2_SEGMENTMANAGER_H
