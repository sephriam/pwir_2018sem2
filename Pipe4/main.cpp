#include<iostream>
#include<unistd.h>
#include<sys/wait.h>

using namespace std;

int main() {

    key_t a;
    int status = 0, mypipe[2];

    if (pipe(mypipe)) { //Tworzymy rurke.
        cerr << "Nie udalo sie utworzyc rurki\n";
        return 1;
    }

    do {
        if (WEXITSTATUS(status) != 0) {
            std::cout << "Użytkownik nacisnął CANCEL\n";
        }

        pid_t pid = fork();

        if (pid == 0) {
            close(mypipe[0]);
            dup2(STDOUT_FILENO, mypipe[1]);
            //Odtąd STDIN_FILENO to to samo co mypipe[0].
            execl("/usr/bin/zenity", "zenity", "--entry", nullptr);
            cerr << "Nie udalo sie uruchomic zenity\n";
            return 1;
        }
        close(mypipe[1]);
        {
            ssize_t bytesRead = 1;
            char buff[128];
            while (bytesRead) {
                bytesRead = read(mypipe[0], buff, 120);
                buff[bytesRead] = '\0';
                std::cout << buff;
            }
        }
        wait(&status);
    } while (WEXITSTATUS(status));
    cerr << "Kończę działanie...\n";
    return 0;
}