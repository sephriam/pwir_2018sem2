#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include <unistd.h>

using namespace std;

/* Pomocnicza funkcja do obsługi błędów */
void error_exit(const char *text){
    if(errno==0&&text!=0) cerr<<text<<"\n";
    else perror(text);
    exit(1);
}

const int SIZE = 1024;

char *buf=0;

int main(int argc, char *argv[]){
    if(argc!=2) error_exit("Błędne argumenty");
    /* Generujemy klucz z pliku podanego
       w argumencie programu */
    key_t key=ftok(argv[1],1);
    if(key==-1) error_exit("Błąd ftok");
    /* Tworzymy/otwieramy segment pamieci dzielonej */
    int id=shmget(key,SIZE,IPC_CREAT|0600);
    if(id==-1)
        error_exit("Błąd przy otwieraniu segmentu");
    /* Podczepiamy segment pamięci dzielonej do
       przestrzeni adresowej programu. Odtąd zmienna
       buf pokazuje na początek tego segmentu */
    buf=(char *)shmat(id,0,0);
    if(buf==(char*)-1)
        error_exit("Błąd przy doczepianiu segmentu");
    cout<<"key="<<key<<"  id="<<id
        <<" pid="<<getpid()<<"\n";
    for(;;){
        cout<<"Tekst: ";
        /* Tutaj oczyszczamy bufor wejsciowy ze znaków
           nowej linii, spacji, itd. */
        for(;;){
            char c=cin.get();
            if(isalnum(c)){
                cin.putback(c);
                break;
            }
        }
        /* Wczytujemy pojedyńczą linię bezpośrednio
           do segmentu dzielonego*/
        cin.getline(buf,SIZE-1,'\n');
        /* Wpisanie exit powoduje zakończenie programu */
        if(strcmp(buf,"exit")==0) break;
    }
    return 0;
}