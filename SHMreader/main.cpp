#include <iostream>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

using namespace std;

const int SIZE = 1024;

char *buf = 0;

/* Pomocnicza funkcja do obsługi błędów */
void error_exit(const char *text) {
    if (errno == 0 && text != 0) cerr << text << "\n";
    else perror(text);
    exit(1);
}

/* Pomocnicza funkcja do wyświetlania wiadomości */
bool display_buffer(char *buf) {
    buf[SIZE - 1] = '\0';//Ensuring sanity
    if (fork() == 0) {
        string txt("Wiadomość:\n");
        txt += buf;
        txt += "\n\nCzy chcesz kontynuować?";
        std::cout << txt;
        execl("/usr/bin/zenity", "zenity", "--question", "--text",txt.c_str(), nullptr);
        //execl("/usr/bin/kdialog", "kdialog", "--yesno",
        //      txt.c_str(), NULL);
        return 2;
    }
    int status;
    wait(&status);
    switch (WEXITSTATUS(status)) {
        case 0:
            return true;
        case 1:
            return false;
        default:
            cerr << "Coś poszło nie tak, kończę...\n";
            exit(1);
    }
}


int main(int argc, char *argv[]) {
    if (argc != 2) error_exit("Błędne argumenty");
    /* Generujemy klucz z pliku podanego
       w argumencie programu */
    key_t key = ftok(argv[1], 1);
    if (key == -1) error_exit("Błąd ftok");
    /* Tworzymy/otwieramy segment pamieci dzielonej */
    int id = shmget(key, SIZE, IPC_CREAT | 0600);
    if (id == -1)
        error_exit("Błąd przy otwieraniu segmentu");
    /* Podczepiamy segment pamięci dzielonej do
       przestrzeni adresowej programu. Odtąd zmienna
       buf pokazuje na początek tego segmentu */
    buf = (char *) shmat(id, 0, 0);
    if (buf == (char *) -1)
        error_exit("Błąd przy doczepianiu segmentu");
    cout << "key=" << key << "  id=" << id
         << " pid=" << getpid() << "\n";
    while (display_buffer(buf));
    return 0;
}